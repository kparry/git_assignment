// Program to calculate Factorials
//Code to calculate factorial of a number
import java.util.Scanner;
// Main Class
//The main class 
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;
 
      System.out.println("Enter an integer to calculate it's factorial");
      // Taking Inputs
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();
 // Assure valid entry
 //Test end case
      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {
// Calculate factorial
//Actual logic for calculating factorial
         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
 // This version includes both comments without the errors because I was unsure before
 // whether or not they were to be included
